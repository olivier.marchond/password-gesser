from ChangeCase import ChangeCase

class ToLowerCase(ChangeCase):

    def __init__(self, words):
        self.operation = "lower"
        super().__init__(words)
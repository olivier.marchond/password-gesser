from ToLowerCase import ToLowerCase
from ToUpperCase import ToUpperCase
from ToPascalCase import ToPascalCase
from ToUnicodeCase import ToUnicodeCase


class Engine:

    def __init__(self, words, dates, options):
        self.options = options
        self.words = words
        self.dates = dates
        self.array_possibilities = []
        self.run()

    def run(self):

        list = self.words
        list.extend(self.dates)

        list.extend(ToLowerCase(self.words).possibilities)
        list.extend(ToUpperCase(self.words).possibilities)
        list.extend(ToPascalCase(self.words).possibilities)
        list.extend(ToUnicodeCase(self.words).possibilities)

        self.printPossibilities('', list, 0)
        return

    def printPossibilities(self, firstPart, list, nbParts):
            if nbParts < 5:
                nbParts += 1
                for word in list:
                    print(firstPart + word)
                    listCopy = list[:]
                    listCopy.remove(word)
                    self.printPossibilities(firstPart + word, listCopy, nbParts)

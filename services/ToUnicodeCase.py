from ChangeCase import ChangeCase

class ToUnicodeCase(ChangeCase):

    def __init__(self, words):
        self.operation = "unicode"
        super().__init__(words)
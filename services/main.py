from flask import Flask
from flask import render_template

import sys
sys.path.append('C:\Dev\password-gesser\services')

from Engine import Engine

from datetime import datetime

import locale
locale.setlocale(locale.LC_TIME,'en_US')

app = Flask(__name__)

@app.route('/')
def index():
    return render_template("./index.html")

@app.route('/run')
def appRun():
    e = Engine(["aé", "Cè"], ["18-04-2001"], {"a": True, "b": False, "c": True})
    return index()

if __name__ == '__main__':
    app.debug = True
    app.run()

    # Helpers

    l33tList = ["a", "A", "e", "E", "i", "I", "o", "O", "l", "L", "s", "S", "b", "B", "t", "T", "z", "Z", "g", "G"]

    def turnInL33t(char):
        if char == "a" or char == "A":
            return 4
        if char == "e" or char == "E":
            return 3
        if char == "i" or char == "I":
            return 1
        if char == "o" or char == "O":
            return 0
        if char == "l" or char == "L":
            return 1
        if char == "s" or char == "S":
            return 5
        if char == "b" or char == "B":
            return 8
        if char == "t" or char == "T":
            return 7
        if char == "z" or char == "Z":
            return 2
        if char == "g" or char == "G":
            return 6

    def findAllIndex(str):
        indexList = []
        for i, ltr in enumerate(str):
            if ltr in l33tList:
                indexList.append(i)
        return indexList

    def getAllListCombo(list):
        if len(list) == 0:
            return null
        comboList = []
        for combo in comboList(list[1:]):
            comboList += [combo, combo+[list[0]]]
        return comboList

    def recursiveL33t(indexToL33t, baseWord):
        for index in indexToL33t:
                word = baseWord
                word = word[0:index] + str(turnInL33t(word[index])) + word[index+1:]
                if word not in parts:
                    parts.append(word)
                copyIndexToL33t = indexToL33t[:]
                copyIndexToL33t.remove(index)
                recursiveL33t(copyIndexToL33t, word)


    # Receiving info from webpage & turn it into a part list

    date1 = datetime.strptime('18-04-2001', '%d-%m-%Y')
    date2 = datetime.strptime('25-07-1995', '%d-%m-%Y')

    items = {
        'words': ["azerty-", "qsdfgh-", "wxcvbn-"],
        'dates': [date1, date2]
    }

    parts = items['words'][:]


    # Print all possible combinations from a list

    def useAllL33t():
        for word in items['words'][:]:
            indexToL33t = findAllIndex(word)
            for index in indexToL33t:
                word = word[0:index] + str(turnInL33t(word[index])) + word[index+1:]
            if word not in parts:
                parts.append(word)
        
        print(parts)

    def useOneL33t():
        for word in items['words'][:]:
            baseWord = word
            indexToL33t = findAllIndex(word)
            for index in indexToL33t:
                word = baseWord
                word = word[0:index] + str(turnInL33t(word[index])) + word[index+1:]
                if word not in parts:
                    parts.append(word)

        print(parts)

    def useFullComboL33t():
        for word in items['words'][:]:
            baseWord = word
            indexToL33t = findAllIndex(word)
            recursiveL33t(indexToL33t, baseWord)

        print(parts)

    def fractionDates():
        for date in items['dates'][:]:
            day = str(date.day)
            month = str(date.month)
            year = str(date.year)
            if day not in parts:
                parts.append(day)
                if len(day) == 1:
                    parts.append('0'+day)
            if month not in parts:
                parts.append(month)
                if len(month) == 1:
                    parts.append('0'+month)
            if year not in parts:
                parts.append(year)
                parts.append(year[2:])

        print(parts)
    

    def dateMonthHumanLanguage():
        for date in items['dates'][:]:
            parts.append(datetime.strftime(date,'%B'))
        print(parts)


    def addSpecialCharsBasic():
        parts.extend([".", "$", "!", "?", "*"])
        print(parts)
            

    def addSpecialCharsAdvanced():
        parts.extend(["&", "~", "#", "{", "[", "|", "`", "/", "^", "@", "]", "}", "=", "+", "°", "_", "-", "%", "¨", "£", "§", ":", ";"])
        print(parts)




    # allLowerLetters()
    # allUpperLetters()
    # firstUpperLetter()
    # removeAccents()

    # useOneL33t()
    # useAllL33t()
    # useFullComboL33t()

    # fractionDates()
    # dateMonthHumanLanguage()

    # addSpecialCharsBasic()
    # addSpecialCharsAdvanced()
    
    # print(findAllIndex("abcdeabcde"))
    # print(turnInL33t("A"))
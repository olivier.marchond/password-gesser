import unidecode

class ChangeCase:

    def __init__(self, words):
        self.words = words
        self.possibilities = []
        self.run()

    def run(self):
        for word in self.words[:]:
            match self.operation:
                case "lower":
                    variation = word.lower()
                case "upper":
                    variation = word.upper()
                case "pascal":
                    variation = word.capitalize()
                case "unicode":
                    variation = unidecode.unidecode(word)
            if variation not in self.words:
                self.possibilities.append(variation)
from ChangeCase import ChangeCase

class ToUpperCase(ChangeCase):

    def __init__(self, words):
        self.operation = "upper"
        super().__init__(words)